Objectif de l'application
Nom prenom
doc chaque fonction entree sortie
erreur

Voici le lien vers notre projet déposer sur git : https://gitlab.com/ninochoux/projetcryptagec

Notre application est un code qui reprends le cryptage César.
Notre équipe : Nunes Varela Théo / Rigal Nino / Soumaya Seddiki / Lima Nelson

Nos fonctions :

- verifierAlphanumerique
    Cette fonction demande un message et elle vérifie que ce message ne contient que des lettres majuscule, minuscule, des espaces ou des chiffres.
    Elle renvois ensuite 0 si le message ne convient pas a ces critères et 1 si le message est conforme.

- cryptageCesar
    Cette fonction prends en entré un char[] qui est le message et un entier qui servira de clé. Ensuite elle applique un décalage de la clé en faisant emplacement actuel + clé
    Exemple : a = 1, clé = 3, cryptageCesar(a) renvoit d.
    Si la lettre y est entré, et que la clé dépasse la lettre z, on continue de compter en revenant a la lettre a, cette situation est aussi valable pouyr les nombres.
    La fonction renvois ensuite le mesage crypté.

- DecryptageCesar
    Cette fonction prends en entré un char[] qui est le message et un entier qui servira de clé. Ensuite elle applique un décalage de la clé en faisant emplacement actuel - clé
    Exemple : d = 4, clé = 3, decyptageCesar(d) renvoit a.
    Si la lettre b est entré, et que la clé renvoie à une valeur inférieure à la lettre a, on continue de compter en revenant a la lettre z, cette situation est aussi valable pour les nombres.
    La fonction renvois ensuite le mesage decrypté.

- chiffrer
    la fonction chiffrer prend en entrée un message de type char[] et une clé qui est un entier. 
    Elle appelle verificationAlphanumérique, si la verification retourne 1 alors cryptageCesar est effectué;
    Si elle retourne 0, un message d'erreur s'enclenche disant que les caractères spéciaux ne sont pas autorisés, le cryptage ne s'effectue pas.
    Un affichage a été réalisé dans la fonction pour rendre le main plus clair.

- dechiffrer
    la fonction dechiffrer prend en entrée un message de type char[] et une clé qui est un entier. 
    Elle appelle verificationAlphanumérique, si la verification retourne 1 alors cryptageCesar est effectué;
    Si elle retourne 0, un message d'erreur s'enclenche disant que les caractères spéciaux ne sont pas autorisés, le cryptage ne s'effectue pas.
    Un affichage a été réalisé dans la fonction pour rendre le main plus clair.

Notre application renvois une erreur si elle on rencontre un caractère spécial, nous n'avons pas non plus réussi à gérer les accents
