/*******************************/
/* Vérification Alphanumérique */
/*******************************/
int verifierAlphanumerique(char msg[]);

/**************************/
/* Cryptage méthode César */
/**************************/
char* cryptageCesar(char msg[], int cle);

/****************************/
/* Décryptage méthode César */
/****************************/
char* decryptageCesar(char msg[], int cle);

/**************************/
/* Chiffrer méthode César */
/**************************/
void chiffrer(char msg[], int cle);

/****************************/
/* Déchiffrer méthode César */
/****************************/
void dechiffrer(char msgCrypte[], int cle);
