/******************************************************************************
*  ASR => 4R2.04                                                              *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Nunes Varela Théo                                            *
*                                                                             *
*  Nom-prénom2 : Rigal Nino                                                   *
*                                                                             *
*  Nom-prénom3 : Seddiki Soumaya                                              *
*                                                                             *
*  Nom-prénom4 : Lima Nelson                                                  *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :                                                           *
*                                                                             *
******************************************************************************/

#include <stdio.h>

/*******************************/
/* Vérification Alphanumérique */
/*******************************/
int verifierAlphanumerique(char msg[])
{
    int i = 0;
    while (msg[i] != '\0') 
    {
        if (msg[i] >= 'A' && msg[i] <= 'Z'
            || msg[i] >= 'a' && msg[i] <= 'z'
                || msg[i] >= '0' && msg[i] <= '9'
                    || msg[i] >= 128 && msg[i] <= 134
                        || msg[i] == ' ')
        {
            i++;
        }
        else
        {
            /*printf("0\n");*/
            return 0;
        }
    }
    /*printf("1\n");*/
    return 1;
}

/**************************/
/* Cryptage méthode César */
/**************************/
char* cryptageCesar(char msg[], int cle) {
    
    int i = 0;
    
    while (msg[i] != '\0') 
    {
        if (msg[i] >= 'A' && msg[i] <= 'Z') 
        {    
            if (msg[i] + cle > 'Z')
            {
                msg[i] = msg[i] + cle - 26;
            }
            else
            {
                msg[i] = msg[i] + cle;
            }
        }
        else if (msg[i] >= 'a' && msg[i] <= 'z') 
        {    
            if (msg[i] + cle > 'z')
            {
                msg[i] = msg[i] + cle - 26;
            }
            else
            {
                msg[i] = msg[i] + cle;
            }
        }
        else if (msg[i] >= '0' && msg[i] <= '9')
        {
            if (msg[i] + cle > '9')
            {
                msg[i] = msg[i] + cle - 10;
            }
            else
            {
                msg[i] = msg[i] + cle;
            }
        }
        //prise en compte des espaces
        else if (msg[i] == ' ')
        {
            msg[i] = msg[i];
        }
        i++;
    }
    
    return msg;
}

/****************************/
/* Décryptage méthode César */
/****************************/
char* decryptageCesar(char msg[], int cle)
{
    int i = 0;
    
    while (msg[i] != '\0') 
    {
        if (msg[i] >= 'A' && msg[i] <= 'Z') 
        {    
            if (msg[i] - cle < 'A')
            {
                msg[i] = msg[i] - cle + 26;
            }
            else
            {
                msg[i] = msg[i] - cle;
            }
        }
        else if (msg[i] >= 'a' && msg[i] <= 'z') 
        {    
            if (msg[i] - cle < 'a')
            {
                msg[i] = msg[i] - cle + 26;
            }
            else
            {
                msg[i] = msg[i] - cle;
            }
        }
        else if (msg[i] >= '0' && msg[i] <= '9')
        {
            if (msg[i] - cle < '0')
            {
                msg[i] = msg[i] - cle + 10;
            }
            else
            {
                msg[i] = msg[i] - cle;
            }
        }
        //prise en compte des espaces
        else if (msg[i] == ' ')
        {
            msg[i] = msg[i];
        }
        i++;
    }
    return msg;
}

/**************************/
/* Chiffrer méthode César */
/**************************/

void chiffrer(char msg[], int cle)
{
    if (verifierAlphanumerique(msg) == 1)
    {
        printf("%s\n",cryptageCesar(msg, cle));
    }
    else
    {
        printf("\nErreur : Les caractères spéciaux ne sont pas autorisés\n");
    }
}

/****************************/
/* Déchiffrer méthode César */
/****************************/
void dechiffrer(char msgCrypte[], int cle)
{
    if (verifierAlphanumerique(msgCrypte) == 1)
    {
        printf("%s\n", decryptageCesar(msgCrypte, cle));
    }
    else
    {
        printf("\nErreur : Les caractères spéciaux ne sont pas autorisés\n");
    }
}

/********/
/* Main */
/********/

int main(){
    /* Création des messages pour les tests/démonstrations */
    char msgOk[] = "abc xyz 012 789";
    char msgCrypte[] = "def abc 345 012";
    char msgNotOk[] = "abc xyz 012 789+";
    int cle = 3;/* On défini ici la clé de chiffrement */

    /* Appel des fonctions */
    printf("Essayons de chiffrer : %s, on obtiens : ", msgOk);
    chiffrer(msgOk, cle);
    printf("\n");
    printf("En déchiffrant %s on obtiens : ", msgCrypte);
    dechiffrer(msgCrypte, cle);
    printf("\n");
    printf("Essayons de chiffrer : %s, qui contient un caractère spécial, on obtiens : ", msgNotOk);
    chiffrer(msgNotOk,cle);
    printf("\n");
    return 0;
}
